package com.ninja.notificationcentre.interfaces;

public interface ImageInterface {
    public void afterImageSet(Boolean isUpdated, Exception e);
}
