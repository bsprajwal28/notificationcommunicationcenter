package com.ninja.notificationcentre.interfaces;


import android.view.View;

public interface OnClickInterface<T> {
    void onClick(View view, T t);
}
