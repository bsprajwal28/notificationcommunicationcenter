package com.ninja.notificationcentre.activities;

import android.app.Activity;
import android.content.Intent;

import com.google.gson.Gson;
import com.ninja.notificationcentre.functions.NotificationCommonFunction;
import com.ninja.notificationcentre.functions.NotificationUserDetails;
import com.ninja.notificationcentre.model.CommunicationDetail;


import java.util.HashMap;

public class CommunicationNavigator {

    public static void openCommunicationActivity(Activity activity, CommunicationDetail communicationDetail){
        if (activity != null) {
             if(validateDetails(activity,communicationDetail)){
                 populateData(activity, communicationDetail);
                 Intent intent = new Intent(activity,CommunicationCategoryActivity.class);
                 activity.startActivity(intent);
             }
        }
    }

    private static boolean validateDetails(Activity activity, CommunicationDetail communicationDetail) {
        if (communicationDetail == null) {
            NotificationCommonFunction.toastString("Communication data is empty", activity);
            return false;
        } else {
            String baseUrl = communicationDetail.getBaseUrl();
            HashMap<String, String> headerParams = communicationDetail.getHeaderParams();
            int asgardUserId = communicationDetail.getAsgardUserId();

            if (baseUrl == null || baseUrl.isEmpty()) {
                NotificationCommonFunction.toastString("Url is empty", activity);
                return false;
            } else if (headerParams == null || headerParams.isEmpty()) {
                NotificationCommonFunction.toastString("Header params is empty", activity);
                return false;
            } else if (asgardUserId == 0) {
                NotificationCommonFunction.toastString("UserId is empty", activity);
                return false;
            }

            return true;
        }
    }

    private static void populateData(Activity activity,CommunicationDetail communicationDetail){
        Gson gson = new Gson();
        String baseUrl = communicationDetail.getBaseUrl();
        NotificationUserDetails.setBaseUrl(activity,baseUrl);

        HashMap<String,String> headerParams = communicationDetail.getHeaderParams();
        String headerParamsInfo = gson.toJson(headerParams);
        NotificationUserDetails.setCommunicationHeaderParams(activity,headerParamsInfo);

        int asgardUserId = communicationDetail.getAsgardUserId();
        NotificationUserDetails.setAsgardUserId(activity,asgardUserId);
    }
}
