package com.ninja.notificationcentre.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;


import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.ninja.notificationcentre.BR;
import com.ninja.notificationcentre.R;
import com.ninja.notificationcentre.adapters.GenericAdapter;
import com.ninja.notificationcentre.databinding.ActNotificationDetailBinding;
import com.ninja.notificationcentre.functions.NotificationCommonFunction;
import com.ninja.notificationcentre.functions.NotificationConstants;
import com.ninja.notificationcentre.model.NotificationContentEntity;

import java.util.ArrayList;
import java.util.List;

public class NotificationDetailActivity extends AppCompatActivity {
    ActNotificationDetailBinding binding;
    private List<NotificationContentEntity> notificationContentEntityList = new ArrayList<>();
    private GenericAdapter<NotificationContentEntity> contentEntityGenericAdapter;
    private String title;
    private String notificationContentStr;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.act_notification_detail);
        getIntentValues();
        initialiseListeners();
        setBindingAdapter();
        setViewContents();

    }

    private void getIntentValues() {
        Intent intent = getIntent();
        title = intent.getStringExtra("Title");
        notificationContentStr = intent.getStringExtra("NotificationcontentStr");
        Gson gson = new Gson();
        notificationContentEntityList = gson.fromJson(notificationContentStr, new TypeToken<List<NotificationContentEntity>>() {
        }.getType());
        if(notificationContentEntityList!=null&&!notificationContentEntityList.isEmpty()){
        for (NotificationContentEntity notificationContentEntity : notificationContentEntityList) {
            if (notificationContentEntity.getType().equalsIgnoreCase(NotificationConstants.TYPE_IMAGE)) {
                notificationContentEntity.setUrl(notificationContentEntity.getValue());
                notificationContentEntity.setText(null);
            } else if(notificationContentEntity.getType().equalsIgnoreCase(NotificationConstants.TYPE_TEXT)){
                notificationContentEntity.setText(notificationContentEntity.getValue());
                notificationContentEntity.setUrl(null);
            }
        }
        }
        else {
            NotificationCommonFunction.toastString("No Notification Contents available",this);
        }

    }

    private void initialiseListeners() {
        binding.appBarHeader.ivRefreshInTB.setVisibility(View.GONE);
        binding.appBarHeader.tvToolBarTitle.setText("Notifications");
        binding.tvTitle.setText(title);
    }

    private void setBindingAdapter() {
        binding.rvDetail.setLayoutManager(new LinearLayoutManager(this));
        contentEntityGenericAdapter = new GenericAdapter<>(notificationContentEntityList, R.layout.adapter_notification_detail, BR.notificationContent, this);
        binding.setGenericAdapter(contentEntityGenericAdapter);
        contentEntityGenericAdapter.notifyDataSetChanged();
    }


    private void setViewContents() {
        binding.appBarHeader.imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NotificationDetailActivity.super.onBackPressed();
            }
        });
    }


}
