package com.ninja.notificationcentre.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.ninja.notificationcentre.BR;
import com.ninja.notificationcentre.R;
import com.ninja.notificationcentre.adapters.GenericAdapter;
import com.ninja.notificationcentre.databinding.ActNotificationBinding;
import com.ninja.notificationcentre.databinding.BottomsheetFilterTypeBinding;
import com.ninja.notificationcentre.functions.NotificationCommonFunction;
import com.ninja.notificationcentre.functions.NotificationConstants;
import com.ninja.notificationcentre.functions.NotificationUserDetails;
import com.ninja.notificationcentre.interfaces.OnClickInterface;
import com.ninja.notificationcentre.model.FilterResponseApi;
import com.ninja.notificationcentre.model.NotificationEntity;
import com.ninja.notificationcentre.model.NotificationRequestDTO;
import com.ninja.notificationcentre.model.NotificationResponseApi;
import com.ninja.notificationcentre.model.ScreenUtils;
import com.ninja.notificationcentre.model.SubcategoryEntity;
import com.ninja.notificationcentre.restClient.RestClientImplementation;
import com.ninja.notificationcentre.restClient.RestClientInterface;
import com.ninja.notificationcentre.restClient.UrlBuilder;


import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class NotificationActivity extends AppCompatActivity {
    private ActNotificationBinding binding;
    private GenericAdapter<NotificationEntity> notificationAdapter;
    private GenericAdapter<SubcategoryEntity> filterAdapter;
    public String GET_NOTIFICATION_TAG = "getNotificationTag";
    public String GET_FILTER_TAG = "getFilterTag";
    private List<NotificationEntity> notificationEntityList = new ArrayList<>();
    private NotificationRequestDTO notificationRequestDTO = new NotificationRequestDTO();
    private List<String> filterDataEntities = new ArrayList<>();
    private List<SubcategoryEntity> subcategoryEntityList = new ArrayList<>();
    private BottomSheetDialog bottomSheetDialog;
    String categoryId = "";
    int asgardUserId=0;
    private int fetchOffset = 0;
    private int fetchLimit = NotificationConstants.DATA_FETECHED_PER_PULL;
    private boolean isFeedExists = true;
    private boolean loading;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.act_notification);
        getIntentValues();
        addRequestData();
        initialiseListeners();
        setBindingAdapter();
        setViewContents();

    }


    private BottomSheetDialog getBottomSheetInstance() {
        if (bottomSheetDialog == null)
            bottomSheetDialog = new BottomSheetDialog(this, R.style.BottomSheetDialog);
        return bottomSheetDialog;
    }

    private void getIntentValues() {

        Intent intent = getIntent();
        categoryId = intent.getStringExtra("categoryId");
        asgardUserId = NotificationUserDetails.getAsgardUserId(this);
    }

    private void addRequestData() {
        notificationRequestDTO.setCategoryId(categoryId);
        notificationRequestDTO.setUserId(asgardUserId);
    }

    @Override
    protected void onResume() {
        super.onResume();
        fetchNotificationData(false, notificationRequestDTO);
    }

    private void initialiseListeners() {
        binding.appBarHeader.tvToolBarTitle.setText("Notifications");
        binding.appBarHeader.ivFilter.setVisibility(View.VISIBLE);
        binding.rvNotification.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                final LinearLayoutManager linearLayoutManager = (LinearLayoutManager)recyclerView.getLayoutManager();
                int totalItemCount = linearLayoutManager.getItemCount();
                int lastVisibleItem = linearLayoutManager
                        .findLastVisibleItemPosition();
                boolean loadMore = lastVisibleItem == totalItemCount - 1;

                if (!loading && loadMore && isFeedExists) {
                    fetchOffset = fetchOffset + fetchLimit;
                    loading = true;
                    fetchNotificationData(true,notificationRequestDTO);
                }
            }
        });
    }

    private void setBindingAdapter() {
        binding.rvNotification.setLayoutManager(new LinearLayoutManager(this));
        notificationAdapter = new GenericAdapter<>(new ArrayList<NotificationEntity>(), R.layout.adapter_notification_display, BR.notificationData, this);
        binding.setGenericAdapter(notificationAdapter);
        notificationAdapter.notifyDataSetChanged();

    }


    private void setViewContents() {
        binding.appBarHeader.imgBack.setOnClickListener(onClick);
        binding.appBarHeader.ivRefreshInTB.setOnClickListener(onClick);
        binding.appBarHeader.ivFilter.setOnClickListener(onClick);

    }

    @Override
    public void onBackPressed() {
        NotificationCommonFunction.startCommunicationActivity(NotificationActivity.this);
    }

    private void fetchNotificationData(final boolean isLoadMore, NotificationRequestDTO notificationparamsDTO) {
        RestClientImplementation.getInstance().cancelRequest(GET_NOTIFICATION_TAG);
        binding.rlErrorHolder.setVisibility(View.GONE);
        if(isLoadMore){
            binding.progressBarSmall.setVisibility(View.VISIBLE);
        }else {
            notificationEntityList.clear();
            fetchOffset=0;
            binding.rlShimmerHolder.setVisibility(View.VISIBLE);
        }
        if (!NotificationCommonFunction.isNetworkAvailable(NotificationActivity.this)) {
            NotificationCommonFunction.toastString(NotificationConstants.NO_INTERNET_CONNECTION, NotificationActivity.this);
            binding.rlShimmerHolder.setVisibility(View.GONE);
            binding.progressBarSmall.setVisibility(View.GONE);
            return;
        }

        UrlBuilder urlBuilder = UrlBuilder.initUIBuilder(this);
        String url = urlBuilder.postnotification(fetchOffset,fetchLimit);

        JSONObject postparams;
        postparams = RestClientImplementation.getJsonObjectAsParams(notificationparamsDTO);

        RestClientImplementation.getInstance().postJsonObjectAndGetJsonObjectResponse(GET_NOTIFICATION_TAG, url, postparams, new RestClientInterface() {
            @Override
            public void onResponse(boolean success, Object response) {
                Log.d("Tag", response.toString());
                binding.rlShimmerHolder.setVisibility(View.GONE);
                binding.progressBarSmall.setVisibility(View.GONE);
                isFeedExists=true;
                loading=false;
                if (success) {
                    handleSuccessResponse(response);
                } else {
                    handleErrorResponse(response);
                }
            }
        }, this);
    }

    private void handleSuccessResponse(Object response) {
        NotificationResponseApi notificationResponseApi;
        Gson gson = new Gson();
        notificationResponseApi = gson.fromJson(response.toString(), new TypeToken<NotificationResponseApi>() {
        }.getType());
        if (notificationResponseApi.getNotificationEntityList() != null && !notificationResponseApi.getNotificationEntityList().isEmpty()) {
            for (NotificationEntity notificationEntity : notificationResponseApi.getNotificationEntityList()) {
                notificationEntity.setOnClickInterface(onClickInterface);
            }
            List<NotificationEntity> notificationDataLists = notificationResponseApi.getNotificationEntityList();
            notificationEntityList.addAll(notificationDataLists);
            if(notificationEntityList.size()<fetchLimit){
                isFeedExists =false;
            }
            notificationAdapter.setAdapterList(notificationEntityList);
            notificationAdapter.notifyDataSetChanged();
        } else {
            notificationAdapter.setAdapterList(new ArrayList<NotificationEntity>());
            notificationAdapter.notifyDataSetChanged();
            binding.rlErrorHolder.setVisibility(View.VISIBLE);
            //NotificationCommonFunction.toastString("No notification available", this);
        }

    }

    private void handleErrorResponse(Object response) {
        NotificationResponseApi errorEntity;
        errorEntity = new Gson().fromJson(response.toString(), NotificationResponseApi.class);
        if (errorEntity.getMessage() != null) {
            if (errorEntity.getErrorCode() == "401") {
                NotificationCommonFunction.toastString("Authentication Error", this);
            } else {
                NotificationCommonFunction.toastString(errorEntity.getMessage(), this);
            }

        } else {
            NotificationCommonFunction.toastString(NotificationConstants.NO_INTERNET_CONNECTION, this);
        }
    }

    private View.OnClickListener onClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            int id = view.getId();
            if (id == R.id.imgBack) {
                NotificationCommonFunction.startCommunicationActivity(NotificationActivity.this);
            } else if (id == R.id.ivRefreshInTB) {
                fetchNotificationData(false, notificationRequestDTO);
            } else if (id == R.id.ivFilter) {
                openFilterDialog();
            } else if (id == R.id.ivFilterClose) {
                closeBottomSheet();
            } else if (id == R.id.tvApplyButton) {
                if (filterDataEntities != null && !filterDataEntities.isEmpty()) {
                    NotificationRequestDTO filterRequestDTO = new NotificationRequestDTO();
                    filterRequestDTO.setUserId(asgardUserId);
                    filterRequestDTO.setCategoryId(categoryId);
                    filterRequestDTO.setSubcategoryIds(filterDataEntities);
                    fetchNotificationData(false, filterRequestDTO);
                    closeBottomSheet();
                } else {
                    closeBottomSheet();
                }
            }
        }
    };


    private void closeBottomSheet() {
        if (bottomSheetDialog != null) {
            bottomSheetDialog.dismiss();
        }
    }

    private void openFilterDialog() {
        try {

            bottomSheetDialog = getBottomSheetInstance();
            View sheetView = getLayoutInflater().inflate(R.layout.bottomsheet_filter_type, null);
            BottomsheetFilterTypeBinding bottomsheetFilterTypeBinding = DataBindingUtil.bind(sheetView);
            if (bottomsheetFilterTypeBinding != null) {
                filterAdapter = new GenericAdapter<>(subcategoryEntityList, R.layout.adapter_filter_type, BR.filterInfo, this);
                bottomsheetFilterTypeBinding.setGenericAdapter(filterAdapter);
                filterAdapter.notifyDataSetChanged();
                setFilter(bottomsheetFilterTypeBinding);
                bottomsheetFilterTypeBinding.ivFilterClose.setOnClickListener(onClick);
                bottomsheetFilterTypeBinding.tvApplyButton.setOnClickListener(onClick);
            }
            bottomSheetDialog.setContentView(sheetView);

            Field mBehaviorField = bottomSheetDialog.getClass().getDeclaredField("behavior");
            mBehaviorField.setAccessible(true);
            BottomSheetBehavior behavior = (BottomSheetBehavior) mBehaviorField.get(bottomSheetDialog);
            ScreenUtils screenUtils = new ScreenUtils(this);
            behavior.setPeekHeight(screenUtils.getHeight());
            behavior.setState(BottomSheetBehavior.STATE_DRAGGING);
            bottomSheetDialog.setCanceledOnTouchOutside(false);
            bottomSheetDialog.show();

        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }


    }

    private void setFilter(final BottomsheetFilterTypeBinding bottomsheetFilterTypeBinding) {
        RestClientImplementation.getInstance().cancelRequest(GET_FILTER_TAG);
        bottomsheetFilterTypeBinding.rlLoader.setVisibility(View.VISIBLE);
        subcategoryEntityList.clear();
        if(!NotificationCommonFunction.isNetworkAvailable(NotificationActivity.this)) {
            NotificationCommonFunction.toastString(NotificationConstants.NO_INTERNET_CONNECTION, NotificationActivity.this);
            bottomsheetFilterTypeBinding.rlLoader.setVisibility(View.GONE);
            return;
        }
        UrlBuilder urlBuilder = UrlBuilder.initUIBuilder(this);
        String url = urlBuilder.getFilterData(categoryId);
        RestClientImplementation.getInstance().getJsonObjectRequest(url, new RestClientInterface() {
            @Override
            public void onResponse(boolean success, Object response) {
                if(success){
                    bottomsheetFilterTypeBinding.rlLoader.setVisibility(View.GONE);
                    FilterResponseApi filterResponseApi;
                    Gson gson = new Gson();
                    List<SubcategoryEntity> responselist;
                    filterResponseApi = gson.fromJson(response.toString(), new TypeToken<FilterResponseApi>(){}.getType());
                    responselist = filterResponseApi.getSubcategoryEntityList();
                    if(responselist==null){
                        //NotificationCommonFunction.toastString("N0 filter data available",NotificationActivity.this);
                        bottomsheetFilterTypeBinding.rlErrorMsg.setVisibility(View.VISIBLE);
                        bottomsheetFilterTypeBinding.rvFilterData.setVisibility(View.GONE);
                        filterAdapter.setAdapterList((new ArrayList<SubcategoryEntity>()));
                        filterAdapter.notifyDataSetChanged();
                    }
                    else {
                        subcategoryEntityList.addAll(responselist);
                        bottomsheetFilterTypeBinding.rvFilterData.setVisibility(View.VISIBLE);
                        for (SubcategoryEntity subcategoryEntity : subcategoryEntityList) {
                            filterDataEntities.clear();
                            subcategoryEntity.setOnClickInterface(onFilterClickInterface);
                        }
                        filterAdapter.setAdapterList(subcategoryEntityList);
                        filterAdapter.notifyDataSetChanged();
                    }

                }
                else {
                    bottomsheetFilterTypeBinding.rlLoader.setVisibility(View.GONE);
                    FilterResponseApi errorEntity;;
                    errorEntity = new Gson().fromJson(response.toString(),FilterResponseApi.class);
                    if (errorEntity.getMessage() != null) {
                        if (errorEntity.getErrorCode() == "401") {
                            NotificationCommonFunction.toastString("Authentication Error",NotificationActivity.this);
                        }
                        else {
                            NotificationCommonFunction.toastString(errorEntity.getMessage(), NotificationActivity.this);
                        }

                    } else{
                        NotificationCommonFunction.toastString(NotificationConstants.NO_INTERNET_CONNECTION, NotificationActivity.this);
                    }
                }


            }
        },this,GET_FILTER_TAG);



    }

    private OnClickInterface<NotificationEntity> onClickInterface = new OnClickInterface<NotificationEntity>() {
        @Override
        public void onClick(View view, NotificationEntity notificationEntity) {
            if (view.getId() == R.id.notificationCard) {
                Bundle bundle = new Bundle();
                Gson gson = new Gson();
                String ContentStr = gson.toJson(notificationEntity.getNotificationContentEntityList());
                bundle.putString("NotificationcontentStr", ContentStr);
                bundle.putString("Title", notificationEntity.getDescription());
                NotificationCommonFunction.commonStartActivity(NotificationActivity.this, NotificationDetailActivity.class, bundle, false);
            }
        }
    };


   private OnClickInterface<SubcategoryEntity> onFilterClickInterface = new OnClickInterface<SubcategoryEntity>() {
       @Override
       public void onClick(View view, SubcategoryEntity subcategoryEntity) {
           if (view.getId() == R.id.rbSort) {
               if (!subcategoryEntity.isIschecked()) {
                   subcategoryEntity.setIschecked(true);
                   filterDataEntities.add(subcategoryEntity.getId());
               } else {
                   subcategoryEntity.setIschecked(false);
                   filterDataEntities.remove(subcategoryEntity.getId());
               }
           }
       }
   };

}
