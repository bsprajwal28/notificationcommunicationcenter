package com.ninja.notificationcentre.activities;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.ninja.notificationcentre.BR;
import com.ninja.notificationcentre.R;
import com.ninja.notificationcentre.adapters.GenericAdapter;
import com.ninja.notificationcentre.databinding.ActCommunicationCategoryBinding;
import com.ninja.notificationcentre.functions.NotificationCommonFunction;
import com.ninja.notificationcentre.functions.NotificationConstants;
import com.ninja.notificationcentre.interfaces.OnClickInterface;
import com.ninja.notificationcentre.model.CommunicationCategoryApi;
import com.ninja.notificationcentre.model.CommunicationEntity;
import com.ninja.notificationcentre.restClient.RestClientImplementation;
import com.ninja.notificationcentre.restClient.RestClientInterface;
import com.ninja.notificationcentre.restClient.UrlBuilder;

import java.util.ArrayList;
import java.util.List;

public class CommunicationCategoryActivity extends AppCompatActivity {
    private ActCommunicationCategoryBinding binding;
    private GenericAdapter<CommunicationEntity> communicationAdapter;
    private List<CommunicationEntity> communicationEntityList = new ArrayList<>();
    public String GET_CATEGORY_TAG = "getCategoryTag";
    private int fetchOffset = 0;
    private int fetchLimit = NotificationConstants.DATA_FETECHED_PER_PULL;
    private boolean isFeedExists = true;
    private boolean loading;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.act_communication_category);
        setBindingAdapter();
        setViewContents();
        initialiseListeners();
    }

    @Override
    protected void onResume() {
        super.onResume();
        fetchData(false);
    }

    private void setViewContents() {

        binding.appBarHeader.tvToolBarTitle.setText("Categories");
    }

    private void setBindingAdapter() {
        binding.rvNotification.setLayoutManager(new LinearLayoutManager(this));
        communicationAdapter= new GenericAdapter<>(new ArrayList<CommunicationEntity>(),R.layout.adapter_communication_options, BR.communicationCategory,this);
        binding.setGenericAdapter(communicationAdapter);
        communicationAdapter.notifyDataSetChanged();

    }

    private void initialiseListeners() {
        binding.appBarHeader.imgBack.setOnClickListener(onClick);
        binding.appBarHeader.ivRefreshInTB.setOnClickListener(onClick);
        binding.rvNotification.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if(dy > 0) {
                    //If is scrolling down..
                    final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                    int totalItemCount = linearLayoutManager.getItemCount();
                    int lastVisibleItem = linearLayoutManager
                            .findLastVisibleItemPosition();
                    boolean loadMore = lastVisibleItem == totalItemCount - 1;

                    if (!loading && loadMore && isFeedExists) {
                        fetchOffset = fetchOffset + fetchLimit;
                        loading = true;
                        fetchData(true);
                    }
                }
            }
        });
    }

    private void fetchData(final boolean isLoadMore) {
        RestClientImplementation.getInstance().cancelRequest(GET_CATEGORY_TAG);
        if(isLoadMore){
            binding.progressBarSmall.setVisibility(View.VISIBLE);
        }else {
            communicationEntityList.clear();
            fetchOffset=0;
            binding.rlLoader.setVisibility(View.VISIBLE);
        }
        if (!NotificationCommonFunction.isNetworkAvailable(CommunicationCategoryActivity.this)) {
            NotificationCommonFunction.toastString(NotificationConstants.NO_INTERNET_CONNECTION, CommunicationCategoryActivity.this);
            binding.rlLoader.setVisibility(View.GONE);
            binding.progressBarSmall.setVisibility(View.GONE);
            return;
        }
        UrlBuilder urlBuilder = UrlBuilder.initUIBuilder(this);
        String url=urlBuilder.getCategories(fetchOffset,fetchLimit);
        //String url ="https://api.myjson.com/bins/15b8b8";
        RestClientImplementation.getInstance().getJsonObjectRequest(url, new RestClientInterface() {
            @Override
            public void onResponse(boolean success, Object response) {
                Log.d("Tag", response.toString());
                binding.rlLoader.setVisibility(View.GONE);
                binding.progressBarSmall.setVisibility(View.GONE);
                isFeedExists=true;
                loading=false;
                if (success) {
                    handleSuccessResponse(response);
                }
                else {
                    handleErrorResponse(response);
                }

            }
        },this,GET_CATEGORY_TAG);
    }

    private void handleSuccessResponse(Object response){

        CommunicationCategoryApi communicationCategoryApi;
        Gson gson = new Gson();

        communicationCategoryApi= gson.fromJson(response.toString(),new TypeToken<CommunicationCategoryApi>(){}.getType());

        communicationEntityList = communicationCategoryApi.getCommunicationEntityList();

        if(!communicationEntityList.isEmpty()&& communicationEntityList!=null){
            for(CommunicationEntity communicationEntity:communicationEntityList){
                communicationEntity.setOnClickInterface(onClickInterface);
            }
            if(communicationEntityList.size()<fetchLimit){
                isFeedExists =false;
            }
            communicationAdapter.setAdapterList(communicationEntityList);
            communicationAdapter.notifyDataSetChanged();
        }
        else {
            communicationAdapter.setAdapterList(new ArrayList<CommunicationEntity>());
            communicationAdapter.notifyDataSetChanged();
            NotificationCommonFunction.toastString("No category lists available",this);

        }
    }

    private void handleErrorResponse(Object response){
        CommunicationCategoryApi errorEntity;
        errorEntity = new Gson().fromJson(response.toString(),CommunicationCategoryApi.class);
        if (errorEntity.getMessage() != null) {
        if (errorEntity.getErrorCode() == "401") {
            NotificationCommonFunction.toastString("Authentication Error",this);
        }
        else {
            NotificationCommonFunction.toastString(errorEntity.getMessage(), this);
        }

    } else{
        NotificationCommonFunction.toastString(NotificationConstants.NO_INTERNET_CONNECTION, this);
    }

}


    private View.OnClickListener onClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            int id = view.getId();
            if (id == R.id.imgBack) {
                CommunicationCategoryActivity.super.onBackPressed();
            } else if (id == R.id.ivRefreshInTB) {
                fetchData(false);
            }
        }
    };

    private OnClickInterface<CommunicationEntity> onClickInterface = new OnClickInterface<CommunicationEntity>() {
        @Override
        public void onClick(View view, CommunicationEntity communicationEntity) {
            if (view.getId() == R.id.categoryCard) {
                String categoryId = "";
                Bundle bundle = new Bundle();
                categoryId = communicationEntity.getId();
                bundle.putString("categoryId", categoryId);
                NotificationCommonFunction.commonStartActivity(CommunicationCategoryActivity.this, NotificationActivity.class, bundle, false);
            }
        }
    };
}




