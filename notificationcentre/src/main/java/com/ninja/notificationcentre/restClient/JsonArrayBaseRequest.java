package com.ninja.notificationcentre.restClient;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;

public class JsonArrayBaseRequest extends JsonArrayRequest {

    public JsonArrayBaseRequest(int get, JSONArray s, String url, Response.Listener<JSONArray> listener, Response.ErrorListener errorListener, int i1, int i) {
        super(get, url, s, listener, errorListener);
        setRetryPolicy(new DefaultRetryPolicy(
                10000, 3,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }

    public JsonArrayBaseRequest(String url, Response.Listener<JSONArray> listener, Response.ErrorListener errorListener, int timeOut, int retries) {
        super(url, listener, errorListener);
        setRetryPolicy(new DefaultRetryPolicy(
                timeOut, retries,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }

    @Override
    protected Response<JSONArray> parseNetworkResponse(NetworkResponse response) {
        return super.parseNetworkResponse(response);
    }

}