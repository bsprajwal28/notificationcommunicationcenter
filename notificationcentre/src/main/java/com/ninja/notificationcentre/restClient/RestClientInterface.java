package com.ninja.notificationcentre.restClient;

public interface RestClientInterface {
    void onResponse(boolean success, Object response);
}
