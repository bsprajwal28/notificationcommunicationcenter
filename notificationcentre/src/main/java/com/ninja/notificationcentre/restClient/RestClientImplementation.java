package com.ninja.notificationcentre.restClient;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.ninja.notificationcentre.functions.NotificationCommonFunction;
import com.ninja.notificationcentre.functions.NotificationUserDetails;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class RestClientImplementation {
    private static RequestQueue queue;

    public synchronized static RestClientImplementation getInstance(){
        return new RestClientImplementation();
    }

    public void getJsonObjectRequest(String partial_url, final RestClientInterface restClientInterface , final Context context, String TAG) {

        queue = VolleySingleton.getInstance(context).getRequestQueue();
        String url = partial_url;
        JsonBaseRequest jsonBaseRequest = new JsonBaseRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        restClientInterface.onResponse(true,response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                handleErrorResponse(error,restClientInterface, context);
            }
        }, 30000, 0) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> params = getHeaderParams(context);
                return params;
            }
        };
        jsonBaseRequest.setTag(TAG);
        queue.add(jsonBaseRequest);
    }

    private void handleErrorResponse(VolleyError error, RestClientInterface restClientInterface, Context context) {
        if (error.networkResponse != null && error.networkResponse.data != null) {
            try {
                JSONObject errorResponse = new JSONObject(new String(error.networkResponse.data));
                restClientInterface.onResponse(false,errorResponse);
            } catch (JSONException e) {
                restClientInterface.onResponse(false,e.toString());
            }
        } else {
            try{
                restClientInterface.onResponse(false,error.toString());
            } catch (Exception e){
                NotificationCommonFunction.toastString("Please try again after sometime", context);
            }
        }
    }

    public void cancelRequest(String tagName) {
        if (queue != null)
            queue.cancelAll(tagName);
    }

    private synchronized static HashMap<String, String> getHeaderParams(Context context) {
        Gson gson = new Gson();
        String headerParamsValue = NotificationUserDetails.getCommunicationHeaderParams(context);
        HashMap<String,String> headerParams = gson.fromJson(headerParamsValue, new TypeToken<HashMap<String,String>>() {
        }.getType());
        return headerParams ;
    }

    public void getJsonArrayRequest(String url, final RestClientInterface restClientInterface,final Context context,String TAG) {
        queue = VolleySingleton.getInstance(context).getRequestQueue();

        JsonArrayBaseRequest getRequest = new JsonArrayBaseRequest(url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Log.e("onResponse", response.toString());
                restClientInterface.onResponse(true,response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                handleErrorResponse(error,restClientInterface, context);
            }
        }, 30000, 0) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> params = getHeaderParams(context);
                return params;
            }
        };
        getRequest.setTag(TAG);
        queue.add(getRequest);
    }

    public void postJsonObjectAndGetJsonObjectResponse(String TAG,String partialUrl, JSONObject postParams, final RestClientInterface genericRestClientInterface,final Context context) {
        queue = VolleySingleton.getInstance(context).getRequestQueue();
        String url = partialUrl;
        JsonBaseRequest postRequest = new JsonBaseRequest(Request.Method.POST, url, postParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("verificationResponse", response.toString());
                        genericRestClientInterface.onResponse(true,response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                handleErrorResponse(error,genericRestClientInterface,context);
            }
        }, 30000, 0) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> params = getHeaderParams(context);
                return params;
            }
        };
        postRequest.setTag(TAG);
        queue.add(postRequest);
    }


    public void postJsonArrayAndGetJsonArrayResponse(String TAG,String url, JSONArray postArray, final RestClientInterface restClientInterface, final Context context){
        queue = VolleySingleton.getInstance(context).getRequestQueue();

        JsonArrayBaseRequest postRequest = new JsonArrayBaseRequest(Request.Method.POST, postArray, url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Log.e("responseStr", response.toString());
                restClientInterface.onResponse(true,response);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                handleErrorResponse(error, restClientInterface, context);
            }
        }, 30000, 0) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> params = getHeaderParams(context);
                return params;
            }
        };
        postRequest.setTag(TAG);
        queue.add(postRequest);
    }


    public static JSONObject getJsonObjectAsParams(Object postObject) {
        JSONObject postParams = null;
        Gson gs = new Gson();
        String postParamString = gs.toJson(postObject);
        try {
            postParams = new JSONObject(postParamString);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        postParams.remove("code");
        postParams.remove("message");
        return postParams;
    }




}
