package com.ninja.notificationcentre.restClient;

import android.content.Context;

import com.ninja.notificationcentre.functions.NotificationUserDetails;

public class UrlBuilder {

    public static UrlBuilder urlBuilder;
    private Context context;

    private UrlBuilder(Context context) {
        this.context = context;
    }

    public static UrlBuilder initUIBuilder(Context context) {
        if (urlBuilder == null)
            urlBuilder = new UrlBuilder(context);
        return urlBuilder;
    }
    private static String getBaseUrl(Context context, String relativeUrl) {
        String url= NotificationUserDetails.getBaseUrl(context);
        return url+relativeUrl;
    }

    public String getCategories(int offset, int limit) {
        return getBaseUrl(context,"/category?offset="+offset+"&limit="+limit);
    }

    public String postnotification(int offset, int limit){
        return getBaseUrl(context,"/notification/findBy?offset="+offset+"&limit="+limit);
    }

    public String getFilterData(String categoryId){
        return getBaseUrl(context,"/subCategory?categoryId="+categoryId);
    }


}
