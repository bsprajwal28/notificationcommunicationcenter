package com.ninja.notificationcentre.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CommunicationCategoryApi {

    private String errorCode;

    @SerializedName("errorMessage")
    private String message;

    private String success;

    @SerializedName("data")
    private List<CommunicationEntity> communicationEntityList;

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public List<CommunicationEntity> getCommunicationEntityList() {
        return communicationEntityList;
    }

    public void setCommunicationEntityList(List<CommunicationEntity> communicationEntityList) {
        this.communicationEntityList = communicationEntityList;
    }
}
