package com.ninja.notificationcentre.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NotificationResponseApi {

    private String errorCode;

    private String errorMessage;

    private String success;

    @SerializedName("data")
    private List<NotificationEntity> notificationEntityList;

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getMessage() {
        return errorMessage;
    }

    public void setMessage(String message) {
        this.errorMessage = message;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public List<NotificationEntity> getNotificationEntityList() {
        return notificationEntityList;
    }

    public void setNotificationEntityList(List<NotificationEntity> notificationEntityList) {
        this.notificationEntityList = notificationEntityList;
    }
}
