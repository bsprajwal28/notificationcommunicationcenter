package com.ninja.notificationcentre.model;

import java.util.HashMap;

public class CommunicationDetail {

    private int asgardUserId;

    private HashMap<String,String> headerParams;

    private String baseUrl;

    public int getAsgardUserId() {
        return asgardUserId;
    }

    public void setAsgardUserId(int asgardUserId) {
        this.asgardUserId = asgardUserId;
    }

    public HashMap<String, String> getHeaderParams() {
        return headerParams;
    }

    public void setHeaderParams(HashMap<String, String> headerParams) {
        this.headerParams = headerParams;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }
}
