package com.ninja.notificationcentre.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FilterResponseApi {

    private String errorCode;

    private String errorMessage;

    private String success;

    @SerializedName("data")
    private List<SubcategoryEntity> subcategoryEntityList;

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getMessage() {
        return errorMessage;
    }

    public void setMessage(String message) {
        this.errorMessage = message;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public List<SubcategoryEntity> getSubcategoryEntityList() {
        return subcategoryEntityList;
    }

    public void setSubcategoryEntityList(List<SubcategoryEntity> subcategoryEntityList) {
        this.subcategoryEntityList = subcategoryEntityList;
    }
}
