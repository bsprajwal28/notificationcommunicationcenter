package com.ninja.notificationcentre.model;

import com.google.gson.annotations.SerializedName;
import com.ninja.notificationcentre.interfaces.OnClickInterface;

import java.util.List;

public class NotificationEntity {

    @SerializedName("name")
    private String description;

    private String id;

    private String userId;

    @SerializedName("subCategoryDTO")
    private SubcategoryEntity subcategoryEntity;

    @SerializedName("content")
    private List<NotificationContentEntity> notificationContentEntityList;

    @SerializedName("updatedAt")
    private String notificationDate;

    private OnClickInterface onClickInterface;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public SubcategoryEntity getSubcategoryEntity() {
        return subcategoryEntity;
    }

    public void setSubcategoryEntity(SubcategoryEntity subcategoryEntity) {
        this.subcategoryEntity = subcategoryEntity;
    }

    public List<NotificationContentEntity> getNotificationContentEntityList() {
        return notificationContentEntityList;
    }

    public void setNotificationContentEntityList(List<NotificationContentEntity> notificationContentEntityList) {
        this.notificationContentEntityList = notificationContentEntityList;
    }

    public String getNotificationDate() {
        return notificationDate;
    }

    public void setNotificationDate(String notificationDate) {
        this.notificationDate = notificationDate;
    }

    public OnClickInterface getOnClickInterface() {
        return onClickInterface;
    }

    public void setOnClickInterface(OnClickInterface onClickInterface) {
        this.onClickInterface = onClickInterface;
    }

}
