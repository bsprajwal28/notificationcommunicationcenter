package com.ninja.notificationcentre.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NotificationRequestDTO {

    private int userId;

    private String categoryId;

    @SerializedName("subCategoryId")
    private List<String> subcategoryIds;


    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public List<String> getSubcategoryIds() {
        return subcategoryIds;
    }

    public void setSubcategoryIds(List<String> subcategoryIds) {
        this.subcategoryIds = subcategoryIds;
    }

}
