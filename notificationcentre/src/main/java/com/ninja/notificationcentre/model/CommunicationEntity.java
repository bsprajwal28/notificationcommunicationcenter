package com.ninja.notificationcentre.model;

import com.ninja.notificationcentre.interfaces.OnClickInterface;

public class CommunicationEntity {

    private String name;

    private String id;

    private OnClickInterface onClickInterface;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public OnClickInterface getOnClickInterface() {
        return onClickInterface;
    }

    public void setOnClickInterface(OnClickInterface onClickInterface) {
        this.onClickInterface = onClickInterface;
    }
}
