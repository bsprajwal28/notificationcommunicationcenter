package com.ninja.notificationcentre.functions;

public class NotificationConstants {

    public static final String NOTIFICATION_USER_PREFERENCE = "Notification_User_Pref";

    public static final String NO_INTERNET_CONNECTION = "No Internet Connection. Kindly check";

    public static final int DATA_FETECHED_PER_PULL = 10;

    public static final String TYPE_TEXT = "TEXT";

    public static final String TYPE_IMAGE = "IMAGE";

}
