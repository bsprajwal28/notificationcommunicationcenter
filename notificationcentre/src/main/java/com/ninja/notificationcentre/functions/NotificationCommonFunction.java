package com.ninja.notificationcentre.functions;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.view.Gravity;
import android.widget.Toast;

import com.ninja.notificationcentre.activities.CommunicationCategoryActivity;
import com.ninja.notificationcentre.activities.NotificationActivity;

public class NotificationCommonFunction {
    public static void toastString(String toastText, Context context) {
        final Toast toast = Toast.makeText(context, toastText, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.TOP, 0, 100);
        toast.show();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                toast.cancel();
            }
        }, 2000);
    }

    public static boolean isNetworkAvailable(Activity act) {
        ConnectivityManager connectivityManager = (ConnectivityManager) act
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager
                .getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static void commonStartActivity(Activity activity, Class className, Bundle bundle, boolean finishActivity) {
        Intent intent = new Intent(activity, className);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        activity.startActivity(intent);
        if (finishActivity)
            activity.finish();
    }

    public static void startCommunicationActivity(Activity act){
        Intent myIntent = new Intent(act, CommunicationCategoryActivity.class);
        myIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        act.startActivity(myIntent);
        act.finish();

    }

    public static void startNotificationAct(Activity activity){
        Intent intent = new Intent(activity, NotificationActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        activity.startActivity(intent);
        activity.finish();
    }

}
