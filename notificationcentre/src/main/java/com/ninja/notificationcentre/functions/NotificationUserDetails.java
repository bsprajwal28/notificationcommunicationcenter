package com.ninja.notificationcentre.functions;

import android.content.Context;
import android.content.SharedPreferences;

import static com.ninja.notificationcentre.functions.NotificationConstants.NOTIFICATION_USER_PREFERENCE;


public class NotificationUserDetails {

    public static void setBaseUrl(Context context, String widgetUrl) {
        SharedPreferences settings = context.getSharedPreferences(NOTIFICATION_USER_PREFERENCE, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("widgetUrl", widgetUrl);
        editor.apply();
    }

    public static String getBaseUrl(Context context) {
        SharedPreferences settings = context.getSharedPreferences(NOTIFICATION_USER_PREFERENCE, 0);
        return (settings.getString("widgetUrl", ""));
    }


    public static void setCommunicationHeaderParams(Context context, String widgetHeaderParams) {
        SharedPreferences settings = context.getSharedPreferences(NOTIFICATION_USER_PREFERENCE, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("NotificationHeaderParams", widgetHeaderParams);
        editor.apply();
    }

    public static String getCommunicationHeaderParams(Context context) {
        SharedPreferences settings = context.getSharedPreferences(NOTIFICATION_USER_PREFERENCE, 0);
        return (settings.getString("NotificationHeaderParams", ""));
    }

    public static void setAsgardUserId(Context context, int asgardUserId){
        SharedPreferences settings = context.getSharedPreferences(NOTIFICATION_USER_PREFERENCE,0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt("asgardId",asgardUserId);
        editor.apply();
    }

    public static int getAsgardUserId(Context context){
        SharedPreferences settings = context.getSharedPreferences(NOTIFICATION_USER_PREFERENCE,0);
        return (settings.getInt("asgardId",0));
    }


}
