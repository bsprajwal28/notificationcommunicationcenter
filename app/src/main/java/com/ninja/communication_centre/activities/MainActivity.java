package com.ninja.communication_centre.activities;

import android.content.Context;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.ninja.communication_centre.R;
import com.ninja.communication_centre.databinding.ActivityMainBinding;
import com.ninja.notificationcentre.activities.CommunicationNavigator;
import com.ninja.notificationcentre.model.CommunicationDetail;

import java.util.HashMap;

public class MainActivity extends AppCompatActivity {
    ActivityMainBinding activityMainBinding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button btn=(Button)findViewById(R.id.btn);
       btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                moveToCommunication();
            }
        });
    }

    private void moveToCommunication() {

        String baseUrl ="http://95.216.39.188:8251/communication";
        HashMap<String,String> headerParams = getHeaderParams(MainActivity.this);
        int asgardUserId = 56212;

        CommunicationDetail communicationDetail = new CommunicationDetail();
        communicationDetail.setAsgardUserId(asgardUserId);
        communicationDetail.setHeaderParams(headerParams);
        communicationDetail.setBaseUrl(baseUrl);

        CommunicationNavigator.openCommunicationActivity(this,communicationDetail);
    }

    public synchronized static HashMap<String, String> getHeaderParams(Context context){
        HashMap<String, String> params = new HashMap<String, String>();
        String creds = String.format("%s:%s", "rameezahamed489", "SHIZA123");
        String auth = "Basic " + Base64.encodeToString(creds.getBytes(), Base64.NO_WRAP);
        params.put("Authorization", auth);
        return params;


    }
}
